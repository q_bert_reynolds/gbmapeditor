﻿Shader "GameBoy/Palette" {
  Properties {
    _BackgroundTiles ("Background Tiles (0-127)", 2D) = "white" {}
    _SpriteTiles ("Sprite Tiles (0-127)", 2D) = "white" {}
    _SharedTiles ("Shared Tiles (128-255)", 2D) = "white" {}
    _Palette ("Palette", 2D) = "grey" {}
    // [Toggle] _ShowColor("Show Color", Float) = 0
    [HideInInspector] _IsSprite("Is Sprite", Float) = 0
  }
  SubShader {
    Tags { "RenderType"="Transparent" "Queue"="Transparent"}
    Blend SrcAlpha OneMinusSrcAlpha
    LOD 100

    Pass {
      CGPROGRAM
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"

      struct appdata {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
        float4 color : COLOR;
      };

      struct v2f {
        float2 uv : TEXCOORD0;
        float4 vertex : SV_POSITION;
        float4 color : COLOR;
      };

      sampler2D _BackgroundTiles;
      float4 _BackgroundTiles_ST;
      sampler2D _SharedTiles;
      sampler2D _SpriteTiles;
      sampler2D _Palette;
      float _ShowColor;
      float _IsSprite;
      
      v2f vert (appdata v) {
        v2f o;
        o.vertex = UnityObjectToClipPos(v.vertex);
        o.uv = TRANSFORM_TEX(v.uv, _BackgroundTiles);
        o.color = v.color;
        return o;
      }
      
      fixed4 frag (v2f i) : SV_Target {
        float2 tileUV = i.uv;
        tileUV.y *= 2;
        fixed4 grey = tex2D(_SharedTiles, tileUV);
        tileUV.y -= 1;
        if (tileUV.y >= 0) {
          if (_IsSprite) grey = tex2D(_SpriteTiles, tileUV);
          else grey = tex2D(_BackgroundTiles, tileUV);
        }
        float2 uv = float2(1-grey.r, 1-i.color.r);
        float4 col = tex2D(_Palette, uv);
        if (_IsSprite && grey.r > 0.9f) {
          col.a = 0;
          grey.a = 0;
        }
        return lerp(grey, col, _ShowColor);
      }
      ENDCG
    }
  }
}
