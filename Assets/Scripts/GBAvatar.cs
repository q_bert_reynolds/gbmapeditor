﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GBAvatar", menuName = "GameBoy/Avatar")]
public class GBAvatar : ScriptableObject {

    public GBStampAnimation walkLeft;
    public GBStampAnimation walkRight;
    public GBStampAnimation walkUp;
    public GBStampAnimation walkDown;

    public GBFrame GetFrame (GBDirectionEnum direction) {
        switch (direction) {
            case GBDirectionEnum.Left:
                if (walkLeft == null || walkLeft.frames[0] == null) return null;
                return walkLeft.frames[0];
            case GBDirectionEnum.Right:
                if (walkRight == null || walkRight.frames[0] == null) return null;
                return walkRight.frames[0];
            case GBDirectionEnum.Up:
                if (walkUp == null || walkUp.frames[0] == null) return null;
                return walkUp.frames[0];
            case GBDirectionEnum.Down:
            default:
                if (walkDown == null || walkDown.frames[0] == null) return null;
                return walkDown.frames[0];
        }
    }

    public int GetTile (int x, int y, GBDirectionEnum direction) {
        GBFrame frame = GetFrame(direction);
        if (frame != null && frame.stamp != null) {
            if (frame.flipX) x = 2-(x%2)-1;
            int t = frame.stamp.GetTile(x%2, y%2);
            if (t >= 0) return t;
        }
        return 0;
    }

    public int GetProps (int x, int y, GBDirectionEnum direction) {
        GBFrame frame = GetFrame(direction);
        if (frame != null && frame.stamp != null) {
            if (frame.flipX) x = 2-(x%2)-1;
            int p = frame.stamp.GetPalette(x%2, y%2);
            if (p >= 0) return p;
        }
        return 0;
    }

    public bool GetXFlip (int x, int y, GBDirectionEnum direction) {
        GBFrame frame = GetFrame(direction);
        if (frame != null && frame.stamp != null) {
            if (frame.flipX) x = 2-(x%2)-1;
            return frame.stamp.GetXFlip(x%2, y%2) ^ frame.flipX;
        }
        return false;
    }

    public override string ToString() {
        string s = "";
        s += ".left: DW GBStampAnimation" + GBMapUtils.ConvertToLabel(walkLeft.name) + "\n";
        s += ".right: DW GBStampAnimation" + GBMapUtils.ConvertToLabel(walkRight.name) + "\n";
        s += ".up: DW GBStampAnimation" + GBMapUtils.ConvertToLabel(walkUp.name) + "\n";
        s += ".down: DW GBStampAnimation" + GBMapUtils.ConvertToLabel(walkDown.name) + "\n";
        return s;
    }
}
