﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBMapText : GBMapObject {

    public string text = "";
    public bool isFill { get { return width > 1 || height > 1 || text.Length > 1; } }

    public static Dictionary<char, int> charMap = new Dictionary<char, int>() {
        { 'ℙ', 1 },             // pitcher
        { 'ℂ', 2 },             // catcher
        { '¹', 3 },             // first
        { '²', 4 },             // second
        { '³', 5 },             // third
        { 'ş', 6 },             // short
        { 'ļ', 7 },             // left
        { 'ç', 8 },             // center
        { 'ŗ', 9 },             // right
        { '◌', 10 },            // dotted circle
        { '⚾', 11 },           // baseball
        { 'ț', 15 },            // to
        { 'ᚠ', 16 },            // age
        { '↵', 30 },            // end/return
        { 'é',  127 },
    };

    public override int GetTile(int x, int y) {
        if (text.Length == 0) return 0;
        char c = text[((height-y-1)*width+x)%text.Length];
        if (charMap.ContainsKey(c)) return charMap[c];
        return (int)c;
    }

    public override string ToString () {
        string s = "\n";
        s += "  DB MAP_OBJ_STAMP"+collisionString+";"+name+"\n";
        s += "  DB " + x + ";x\n";
        s += "  DB " + gby + ";y\n";
        s += "  DW GBStamp" + GBMapUtils.ConvertToLabel(text) + "Text\n";
        return s;
    }

    public GBStamp GetStamp () {
        GBStamp stamp = ScriptableObject.CreateInstance<GBStamp>();
        stamp.name = text + " Text";
        stamp.width = width;
        stamp.height = height;
        stamp.tiles = new int[width*height];
        stamp.palettes = new int[width*height];
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int index = (height-1-y)*width+x;
                stamp.tiles[index] = GetTile(x,y);
                stamp.palettes[index] = palette;
            }
        }
        return stamp;
    }
}
