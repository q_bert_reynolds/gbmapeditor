﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class GBMapChunk : GBMapObject {

    public GBMapChunk north;
    public GBMapChunk east;
    public GBMapChunk south;
    public GBMapChunk west;

    public string northName { get { return north == null ? name : north.name; }}
    public string eastName { get { return east == null ? name : east.name; }}
    public string southName { get { return south == null ? name : south.name; }}
    public string westName { get { return west == null ? name : west.name; }}
    public string northEastName { 
        get {
            if (north != null) return north.eastName;
            if (east != null) return east.northName; 
            return name;
        }
    }
    public string southEastName { 
        get {
            if (south != null) return south.eastName;
            if (east != null) return east.southName; 
            return name;
        }
    }
    public string northWestName { 
        get {
            if (north != null) return north.westName;
            if (west != null) return west.northName; 
            return name;
        }
    }
    public string southWestName { 
        get {
            if (south != null) return south.westName;
            if (west != null) return west.southName; 
            return name;
        }
    }

    public override int width { get { return 32; } }
    public override int height { get { return 32; } }
}
