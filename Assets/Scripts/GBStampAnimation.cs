﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GBFrame {
    public bool flipX = false;
    public GBStamp stamp;
}

[CreateAssetMenu(fileName = "GBStampAnimation", menuName = "GameBoy/Stamp Animation")]
public class GBStampAnimation : ScriptableObject {

    public GBFrame[] frames;

    public override string ToString() {
        string s = "";
        for (int i = 0; i < frames.Length; i++) {
            GBFrame f = frames[i];
            s += "  DB " + (f.flipX ? "1" : "0") + "\n";
            s += "  DW GBStamp" + GBMapUtils.ConvertToLabel(f.stamp.name) + ".tiles\n";
            s += "  DW GBStamp" + GBMapUtils.ConvertToLabel(f.stamp.name) + ".props\n";
        }
        return s;
    }
}
