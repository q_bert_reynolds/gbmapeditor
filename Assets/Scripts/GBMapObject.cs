using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(SortingGroup))]
[ExecuteInEditMode]
public class GBMapObject : MonoBehaviour {

#if UNITY_EDITOR
    void Start () {
        if (Application.isPlaying) return;
        UpdateRect();
    }
#endif

    public int tile;
    public int palette;
    public bool flipX;
    public bool flipY;
    public float uvInset = 0.001f;
    public bool isSprite = false;

    public int props {
        get { 
            if (!isSprite) return palette;
            int prop = palette;
            if (flipX) prop |= (1 << 5);
            if (flipY) prop |= (1 << 6);
            return prop;
        }
    }

    public virtual int x { 
        get { 
            if (isSprite) return Mathf.FloorToInt(transform.localPosition.x*8);
            else return Mathf.FloorToInt(transform.localPosition.x); 
        }
    }
    public virtual int y { 
        get { 
            if (isSprite) return Mathf.FloorToInt(transform.localPosition.y*8);
            else return Mathf.FloorToInt(transform.localPosition.y); 
        }
    }
    public virtual int gby { 
        get { 
            if (isSprite) return 255 - height - y;
            else return 32 - height - y; 
        }
    }
    public virtual int width { 
        get { 
            if (isSprite) return 1;
            else return Mathf.Max(1, Mathf.RoundToInt((transform as RectTransform).sizeDelta.x)); 
        }
    }
    public virtual int height { 
        get { 
            if (isSprite) return 1;
            else return Mathf.Max(1, Mathf.RoundToInt((transform as RectTransform).sizeDelta.y)); 
        }
    }

    public Vector2Int LocationInChunk (GBMapChunk chunk) {
        Vector3 pos = chunk.transform.InverseTransformPoint(transform.position);
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        y = 32 - height - y;
        return new Vector2Int(x, y);
    }

    public virtual int asmSize {
        get {
            if (width > 1 || height > 1) return 7;
            return 5;
        }
    }

    public virtual string collisionString {
        get {
            string s = " | MAP_COLLISION_NONE ";
            Collider2D c = GetComponent<Collider2D>();
            if (c != null) {
                string layerName = LayerMask.LayerToName(gameObject.layer);
                GBData data = GetComponent<GBData>();
                if (data != null) {
                    s = " | " + data.collisionString + " ";
                }
                else if (layerName == "Default") {
                    s = " | MAP_COLLISION_SOLID ";
                }
                else {
                    s = " | MAP_COLLISION_"+layerName.ToUpper()+" ";
                }
            }
            return s;
        }
    }

    public override string ToString () {
        MeshRenderer r = GetComponent<MeshRenderer>();
        string s = "\n  DB MAP_OBJ_";
        s += r.enabled ? "TILE" : "NONE";
        
        if (width > 1 || height > 1) s += "_FILL";
        
        s += collisionString+";"+name+"\n";
        s += "  DB " + x + ";x1\n";
        s += "  DB " + gby + ";y1\n";
        
        if (r.enabled) {
            s += "  DB " + tile + ";tile\n";
            if (isSprite) s += "  DB " + props + ";props\n";
            else s += "  DB " + palette + ";palette\n";
        }
        if (width > 1 || height > 1) {
            s += "  DB " + (x+width) + ";x2\n";
            s += "  DB " + (gby+height) + ";y2\n";
        }
        return s;
    }

    public virtual bool Contains (int x, int y) {
        return (x >= this.x && y >= this.y && x < (this.x + width) && y < (this.y + height));
    }

    public virtual int GetTile(int x, int y) {
        return tile;
    }

    public virtual int GetProps (int x, int y) {
        return palette;
    }

    public virtual int GetPalette (int x, int y) {
        return GetProps(x,y) & 7;//only lowest 3 bits are palette data
    }

    public virtual bool GetXFlip(int x, int y) {
        return isSprite && flipX;
    }

    public virtual bool GetYFlip(int x, int y) {
        return isSprite && flipY;
    }

    public virtual void UpdateRect () {
        Vector3 pos = transform.position;
        if (isSprite) {
            pos.x = Mathf.RoundToInt(pos.x * 8)/8f;
            pos.y = Mathf.RoundToInt(pos.y * 8)/8f;
        }
        else {
            pos.x = Mathf.RoundToInt(pos.x);
            pos.y = Mathf.RoundToInt(pos.y);
        }
        pos.z = 0;
        transform.position = pos;

        RectTransform rect = transform as RectTransform;
        rect.sizeDelta = new Vector2(width, height);
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        if (mesh == null || mesh.vertices == null || mesh.vertices.Length/4 != width*height) CreateMesh();

        BoxCollider2D box = GetComponent<BoxCollider2D>();
        if (box != null) {
            box.size = rect.sizeDelta;
            box.offset = rect.sizeDelta * 0.5f;
        }
    }
    
    void Update() {
        MaterialPropertyBlock props = new MaterialPropertyBlock();
        props.SetFloat("_IsSprite", isSprite ? 1 : 0);
        GetComponent<MeshRenderer>().SetPropertyBlock(props);
    }

    [ContextMenu("Create Mesh")]
    public virtual void CreateMesh () {
        if (width < 0 || height < 0) return;
        RectTransform rect = transform as RectTransform;
        rect.pivot = Vector2.zero;
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.zero;
        rect.sizeDelta = new Vector2(width, height);
        
        float w = 1f / 16f;
        float h = 1f / 16f;
        int vertCount = width*height*4;
        Vector3[] verts = new Vector3[vertCount];
        Vector3[] norms = new Vector3[vertCount];
        Vector2[] uvs = new Vector2[vertCount];
        Color[] colors = new Color[vertCount];
        int[] tris = new int[width*height*6];

        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                int index = row*width+column;
                int quad = index * 4;
                int tri = index * 6;

                verts[quad  ] = new Vector3(column,   row,   0);
                verts[quad+1] = new Vector3(column,   row+1, 0);
                verts[quad+2] = new Vector3(column+1, row+1, 0);
                verts[quad+3] = new Vector3(column+1, row,   0);
                
                norms[quad  ] = Vector3.back;
                norms[quad+1] = Vector3.back;
                norms[quad+2] = Vector3.back;
                norms[quad+3] = Vector3.back;
                
                float p = (float)GetPalette(column, row) / 7f;
                Color c = new Color(p,p,p,1);

                colors[quad  ] = c;
                colors[quad+1] = c;
                colors[quad+2] = c;
                colors[quad+3] = c;

                int id = GetTile(column, row);
                int i = id % 16;
                int j = 15 - id / 16;
                
                uvs[quad  ] = new Vector2(    i*w + uvInset,     j*h + uvInset);
                uvs[quad+1] = new Vector2(    i*w + uvInset, (j+1)*h - uvInset);
                uvs[quad+2] = new Vector2((i+1)*w - uvInset, (j+1)*h - uvInset);
                uvs[quad+3] = new Vector2((i+1)*w - uvInset,     j*h + uvInset);

                if (GetXFlip(column, row)) {
                    Vector2 temp = uvs[quad];
                    uvs[quad] = uvs[quad+3];
                    uvs[quad+3] = temp;
                    temp = uvs[quad+1];
                    uvs[quad+1] = uvs[quad+2];
                    uvs[quad+2] = temp;
                }

                if (GetYFlip(column, row)) {
                    Vector2 temp = uvs[quad];
                    uvs[quad] = uvs[quad+1];
                    uvs[quad+1] = temp;
                    temp = uvs[quad+3];
                    uvs[quad+3] = uvs[quad+2];
                    uvs[quad+2] = temp;
                }

                tris[tri  ] = quad;
                tris[tri+1] = quad+1;
                tris[tri+2] = quad+2;
                tris[tri+3] = quad;
                tris[tri+4] = quad+2;
                tris[tri+5] = quad+3; 
            }
        }

        Mesh mesh = new Mesh();
        mesh.vertices = verts;
        mesh.colors = colors;
        mesh.normals = norms;
        mesh.uv = uvs;
        mesh.triangles = tris;
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public virtual void UpdateUVs () {
        int vertCount = width*height*4;
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        if (mesh == null || mesh.vertices == null || mesh.vertices.Length != vertCount) {
            CreateMesh();
            return;
        }

        Vector2[] uvs = new Vector2[vertCount];

        float w = 1f / 16f;
        float h = 1f / 16f;
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                int index = row*width+column;
                int quad = index * 4;

                int tile = GetTile(column, row);
                int i = tile % 16;
                int j = 15 - tile / 16;
                uvs[quad  ] = new Vector2(    i*w + uvInset,     j*h + uvInset);
                uvs[quad+1] = new Vector2(    i*w + uvInset, (j+1)*h - uvInset);
                uvs[quad+2] = new Vector2((i+1)*w - uvInset, (j+1)*h - uvInset);
                uvs[quad+3] = new Vector2((i+1)*w - uvInset,     j*h + uvInset);

                if (GetXFlip(column, row)) {
                    Vector2 temp = uvs[quad];
                    uvs[quad] = uvs[quad+3];
                    uvs[quad+3] = temp;
                    temp = uvs[quad+1];
                    uvs[quad+1] = uvs[quad+2];
                    uvs[quad+2] = temp;
                }

                if (GetYFlip(column, row)) {
                    Vector2 temp = uvs[quad];
                    uvs[quad] = uvs[quad+1];
                    uvs[quad+1] = temp;
                    temp = uvs[quad+3];
                    uvs[quad+3] = uvs[quad+2];
                    uvs[quad+2] = temp;
                }
            }
        }
        mesh.uv = uvs;
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public virtual void UpdateColors () {
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        if (mesh == null || mesh.vertices == null) {
            CreateMesh();
            return;
        }

        int vertCount = mesh.colors.Length;
        Color[] colors = new Color[vertCount];
        for (int i = 0; i < vertCount; i++) {
            colors[i] = Color.white * palette / 7f;
        }
        mesh.colors = colors;
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }
}