﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GBDirectionEnum {
    Right = 16,
    Left  = 32,
    Up    = 64,
    Down  = 128
}

public class GBDirection : GBData {

    public GBDirectionEnum directionMask = GBDirectionEnum.Down;

    public override string collisionString {
        //TODO: handle more directional types
        get { return "MAP_COLLISION_LEDGE"; }
    }
}
