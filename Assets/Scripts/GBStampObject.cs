using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class GBStampObject : GBMapObject {

    public GBStamp stamp;

    public override int width {
        get {
            if (stamp == null) return base.width;
            int w = Mathf.RoundToInt(base.width / stamp.width) * stamp.width;
            return Mathf.Max(w, stamp.width);
        }
    }

    public override int height {
        get {
            if (stamp == null) return base.height;
            int h = Mathf.RoundToInt(base.height / stamp.height) * stamp.height;
            return Mathf.Max(h, stamp.height);
        }
    }

    public override int asmSize {
        get { 
            if (width != stamp.width || height > stamp.height) return 7;
            return 5;
        }
    }
    
    public override int GetTile (int x, int y) {
        if (stamp != null) {
            int t = stamp.GetTile(x%stamp.width, y%stamp.height);
            if (t >= 0) return t;
        }
        return tile;
    }

    public override int GetProps(int x, int y) {
        if (stamp != null) {
            int p = stamp.GetProps(x%stamp.width, y%stamp.height);
            if (p >= 0) return p;
        }
        return palette;
    }

    public override bool GetXFlip(int x, int y) {
        if (!isSprite) return false;
        if (stamp != null) {
            int p = stamp.GetProps(x%stamp.width, y%stamp.height);
            if (p >= 0) return (p & (1<<5)) != 0;
        }
        return flipX;
    }

    public override bool GetYFlip(int x, int y) {
        if (!isSprite) return false;
        if (stamp != null) {
            int p = stamp.GetProps(x%stamp.width, y%stamp.height);
            if (p >= 0) return (p & (1<<6)) != 0;
        }
        return flipY;
    }

    public override string ToString () {
        if (!GetComponent<Renderer>().enabled) return base.ToString();
        string s = "\n";
        if (width != stamp.width || height != stamp.height) {
            s += "  DB MAP_OBJ_STAMP_FILL"+collisionString+";"+name+"\n";
            s += "  DB " + x + ";x1\n";
            s += "  DB " + gby + ";y1\n";
            s += "  DW GBStamp" + GBMapUtils.ConvertToLabel(stamp.name) + "\n";
            s += "  DB " + (x+width) + ";x2\n";
            s += "  DB " + (gby+height) + ";y2\n";
        }
        else {
            s += "  DB MAP_OBJ_STAMP"+collisionString+";"+name+"\n";
            s += "  DB " + x + ";x\n";
            s += "  DB " + gby + ";y\n";
            s += "  DW GBStamp" + GBMapUtils.ConvertToLabel(stamp.name) + "\n";
        }
        
        return s;
    }
}