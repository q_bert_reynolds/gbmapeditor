﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class GBLineObject : GBMapObject {

    public override int asmSize {
        get { return 7; }
    }

    public override int width { get { return Mathf.RoundToInt((transform as RectTransform).sizeDelta.x); } }
    public override int height { get { return Mathf.RoundToInt((transform as RectTransform).sizeDelta.y); } }

    public override string ToString () {
        string s = "";
        s += "\nDB MAP_OBJ_LINE"+collisionString+";"+name+"\n";
        s += "DB " + x + ";x\n";
        s += "DB " + gby + ";y\n";
        s += "DB " + tile + ";tile\n";
        s += "DB " + palette + ";palette\n";
        s += "DB " + width + ";dx\n";
        s += "DB " + height + ";dy\n";
        return s;
    }

    public override void CreateMesh () {
        RectTransform rect = transform as RectTransform;
        rect.pivot = Vector2.zero;
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.zero;
        rect.sizeDelta = new Vector2(width, height);
        
        Vector2Int[] line = GetLine();

        float w = 1f / 16f;
        float h = 1f / 16f;
        int vertCount = line.Length*4;
        Vector3[] verts = new Vector3[vertCount];
        Vector3[] norms = new Vector3[vertCount];
        Vector2[] uvs = new Vector2[vertCount];
        Color[] colors = new Color[vertCount];
        int[] tris = new int[line.Length*6];

        float p = (float)palette / 7f;
        Color c = new Color(p,p,p,1);
        for (int index = 0; index < line.Length; index++) {
            int quad = index * 4;
            int tri = index * 6;

            Vector2Int pos = line[index];
            verts[quad  ] = new Vector3(pos.x,   pos.y,   0);
            verts[quad+1] = new Vector3(pos.x,   pos.y+1, 0);
            verts[quad+2] = new Vector3(pos.x+1, pos.y+1, 0);
            verts[quad+3] = new Vector3(pos.x+1, pos.y,   0);
                
            norms[quad  ] = Vector3.back;
            norms[quad+1] = Vector3.back;
            norms[quad+2] = Vector3.back;
            norms[quad+3] = Vector3.back;

            colors[quad  ] = c;
            colors[quad+1] = c;
            colors[quad+2] = c;
            colors[quad+3] = c;

            int id = GetTile(pos.x, pos.y);
            int i = id % 16;
            int j = 15 - id / 16;
                
            uvs[quad  ] = new Vector2(    i*w + uvInset,     j*h + uvInset);
            uvs[quad+1] = new Vector2(    i*w + uvInset, (j+1)*h - uvInset);
            uvs[quad+2] = new Vector2((i+1)*w - uvInset, (j+1)*h - uvInset);
            uvs[quad+3] = new Vector2((i+1)*w - uvInset,     j*h + uvInset);

            tris[tri  ] = quad;
            tris[tri+1] = quad+1;
            tris[tri+2] = quad+2;
            tris[tri+3] = quad;
            tris[tri+4] = quad+2;
            tris[tri+5] = quad+3; 
        }

        Mesh mesh = new Mesh();
        mesh.vertices = verts;
        mesh.colors = colors;
        mesh.normals = norms;
        mesh.uv = uvs;
        mesh.triangles = tris;
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    public override void UpdateUVs () {
        Vector2Int[] line = GetLine();
        int vertCount = line.Length*4;
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        if (mesh == null || mesh.vertices == null || mesh.vertices.Length != vertCount) {
            CreateMesh();
            return;
        }

        Vector2[] uvs = new Vector2[vertCount];

        float w = 1f / 16f;
        float h = 1f / 16f;
        for (int index = 0; index < line.Length; index++) {
            int quad = index * 4;
            Vector2Int pos = line[index];
            int tile = GetTile(pos.x, pos.y);
            int i = tile % 16;
            int j = 15 - tile / 16;
            uvs[quad  ] = new Vector2(    i*w + uvInset,     j*h + uvInset);
            uvs[quad+1] = new Vector2(    i*w + uvInset, (j+1)*h - uvInset);
            uvs[quad+2] = new Vector2((i+1)*w - uvInset, (j+1)*h - uvInset);
            uvs[quad+3] = new Vector2((i+1)*w - uvInset,     j*h + uvInset);
        }
        mesh.uv = uvs;
        GetComponent<MeshFilter>().sharedMesh = mesh;
    }

    Vector2Int[] GetLine () {
        List<Vector2Int> line = new List<Vector2Int>();
        int x1 = 0;
        int y1 = 0;
        int x2 = width;
        int y2 = height;
        int dx = x2 - x1;
        int dy = y2 - y1;
        int step = 0;
        int x = 0;
        int y = 0;
        if (Mathf.Abs(dx) > Mathf.Abs(dy)) {
            y = y1;
            for (x = x1; x != x2; x += (int)Mathf.Sign(dx)) {
                step += Mathf.Abs(dy);
                if(step >= Mathf.Abs(dx)) {
                    y += (int)Mathf.Sign(dy);
                    step -= Mathf.Abs(dx);
                }
                line.Add(new Vector2Int(x,y));
            }
        }
        else {
            x = x1;
            for (y = y1; y != y2; y += (int)Mathf.Sign(dy)) {
                step += Mathf.Abs(dx);
                if (step >= Mathf.Abs(dy)) {
                    x += (int)Mathf.Sign(dx);
                    step -= Mathf.Abs(dy);
                }
                line.Add(new Vector2Int(x,y));
            }
        }
        return line.ToArray();
    }
}
