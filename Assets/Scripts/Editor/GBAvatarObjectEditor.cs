﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(GBAvatarObject))]
[CanEditMultipleObjects]
public class GBAvatarObjectEditor : GBMapObjectEditor {

    GBAvatarObject avatarObj { get { return (GBAvatarObject)target; } }

    public override void OnInspectorGUI() {
        if (!avatarObj.isSprite) avatarObj.isSprite = true;

        if (GUI.changed) SnapToGrid();
    }
}
