﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBMapText))]
[CanEditMultipleObjects]
public class GBMapTextEditor : GBMapObjectEditor {

    GBMapText gbMapText { get { return (GBMapText)target; } }

    public override void OnInspectorGUI() {
        gbMapText.text = EditorGUILayout.TextField(gbMapText.text);
        DrawPalettePicker();
        if (GUI.changed) UpdateUVs();
    }

}
