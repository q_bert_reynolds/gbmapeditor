using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBStamp))]
public class GBStampEditor : Editor {

    public static int tile = 0;
    public static int palette = 0;
    public static bool flipX = false;
    public static bool flipY = false;
    public static int props {
        get {
            int prop = palette;
            if (flipX) prop |= (1 << 5);
            if (flipY) prop |= (1 << 6);
            return prop;
        }
    }

    GBStamp stamp { get { return (GBStamp)target; } }

    SerializedProperty tilesProp;
    SerializedProperty palettesProp;

    void OnEnable() {
        tilesProp = serializedObject.FindProperty("tiles");
        palettesProp = serializedObject.FindProperty("palettes");
        Undo.undoRedoPerformed += UndoCallback;
    }

    void OnDisable () {
        Undo.undoRedoPerformed -= UndoCallback;
    }

    void UndoCallback () {
        if (!string.IsNullOrEmpty(GUI.GetNameOfFocusedControl())) DrawStamp(stamp);
        GBStampObject[] stampObjs = FindObjectsOfType<GBStampObject>();
        foreach (GBStampObject obj in stampObjs) {
            if (obj.stamp == stamp) obj.CreateMesh();
        }
    }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        Undo.RecordObject(stamp, "Edit Stamp Size");
        if (stamp.width < 1) stamp.width = 1;
        if (stamp.height < 1) stamp.height = 1;
        if (stamp.tiles == null || stamp.width*stamp.height != stamp.tiles.Length) {
            stamp.tiles = new int[stamp.width*stamp.height];
        }

        if (!stamp.isSprite) EditorGUILayout.LabelField(stamp.hasUniformPalette ? "Uniform Palette" : "Non-Uniform Palette");
        if (stamp.mat == null || stamp.mat.mainTexture == null) return;
        EditStamp();
        stamp.mat.SetFloat("_IsSprite", (stamp.isSprite) ? 1 : 0);
        int t = GBMapEditor.DrawTileMap(stamp.mat, tile);
        if (stamp.isSprite) {
            EditorGUILayout.BeginHorizontal();
            flipX = EditorGUILayout.Toggle("Flip X", flipX);
            flipY = EditorGUILayout.Toggle("Flip Y", flipY);
            EditorGUILayout.EndHorizontal();
        }

        int p = GBMapEditor.DrawPaletteMap(stamp.mat.GetTexture("_Palette"), palette);
        stamp.mat.SetFloat("_IsSprite", 0);

        if (t != tile || p != palette) {
            tile = t;
            palette = p;
            Repaint();
        }
    }

    void EditStamp () {
        Event e = Event.current;
        Rect r = DrawStamp(stamp);

        if (e.type == EventType.MouseDown && e.button == 0 && r.Contains(e.mousePosition)) {
            Undo.RecordObject(stamp, "Edit Stamp Tiles");
            Vector2 p = e.mousePosition;
            int i = Mathf.FloorToInt(stamp.width * (p.x - r.x) / r.width);
            int j = Mathf.FloorToInt(stamp.height * (p.y - r.y) / r.height);
            SerializedProperty tileProp = tilesProp.GetArrayElementAtIndex(j * stamp.width + i);
            tileProp.intValue = tile;
            SerializedProperty paletteProp = palettesProp.GetArrayElementAtIndex(j * stamp.width + i);
            paletteProp.intValue = stamp.isSprite ? props : palette;
            serializedObject.ApplyModifiedProperties();
            Repaint();
            GBStampObject[] stampObjs = FindObjectsOfType<GBStampObject>();
            foreach (GBStampObject obj in stampObjs) {
                if (obj.stamp == stamp) obj.CreateMesh();
            }
        }
    }

    public static Rect DrawStamp (GBStamp stamp, bool flipX = false) {
        stamp.mat.SetFloat("_IsSprite", (stamp.isSprite) ? 1 : 0);
        Rect r = EditorGUILayout.GetControlRect(false, 32*stamp.height); 
        r.width = 32;
        r.height = 32;
        float left = r.x;
        float top = r.y;
        float w = 1f / 16f;
        float h = 1f / 16f;
        for (int row = 0; row < stamp.height; row++) {
            for (int column = 0; column < stamp.width; column++) {
                int i = flipX ? stamp.width-column-1 : column;
                int j = stamp.height-row-1;
                float tile = stamp.GetTile(i, j);
                float x = Mathf.Ceil(tile % 16) / 16f;
                float y = Mathf.Ceil(15 - tile / 16) / 16f;
                float p = (float)stamp.GetPalette(i, j) / 7f;
                Color c = new Color(p,p,p,1);
                Rect s = new Rect(x,y,w,h);
                if (stamp.GetXFlip(i, j) ^ flipX) {
                    s.x += s.width;
                    s.width *= -1;
                }
                if (stamp.GetYFlip(i, j)) {
                    s.y += s.height;
                    s.height *= -1;
                }
                Graphics.DrawTexture(r, stamp.mat.mainTexture, s, 0, 0, 0, 0, c, stamp.mat); 
                r.x += r.width;
            }
            r.x = left;
            r.y += r.height;
        }
        stamp.mat.SetFloat("_IsSprite", 0);
        return new Rect(left, top, r.width * stamp.width, r.height * stamp.height);
    }
}