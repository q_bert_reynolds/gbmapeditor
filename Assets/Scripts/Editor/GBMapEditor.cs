using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using System.IO;
using System.Reflection;

public class GBMapEditor : EditorWindow {

    public static string lastFolder;
    public Color gridColor = Color.black;
    public float uvInset = -1;
    public Material tileMap;
    public int tile;
    public int palette;

    private Material _mat;
    public Material mat {
        get {
            if (_mat == null) _mat = new Material(Shader.Find("Legacy Shaders/Particles/Alpha Blended"));
            return _mat;
        }
    }

    [MenuItem("GameBoy/Map Editor")]
    static void Init() {
        GBMapEditor window = EditorWindow.GetWindow(typeof(GBMapEditor)) as GBMapEditor;
        window.Show();
    }

    void OnGUI() {
        if (string.IsNullOrEmpty(lastFolder)) {
            lastFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
            lastFolder = Directory.GetParent(lastFolder).ToString();
            lastFolder = Path.Combine(lastFolder, "Projects/BeisbolGB/maps");
            lastFolder = Path.GetFullPath(lastFolder);
            // Debug.Log(lastFolder);
        }
        tileMap = EditorGUILayout.ObjectField("Material", tileMap, typeof(Material), false) as Material;
        gridColor = EditorGUILayout.ColorField("Grid Color", gridColor);

        GBMapObject[] objs = Object.FindObjectsOfType<GBMapObject>() as GBMapObject[];
        if (objs.Length == 0) return;

        if (uvInset < 0) uvInset = objs[0].uvInset;
        float t = EditorGUILayout.Slider("UV Inset", uvInset, 0, 0.01f);
        if (t != uvInset) {
            uvInset = t;
            foreach (GBMapObject obj in objs) {
                obj.uvInset = uvInset;
                obj.UpdateUVs();
            }
        }

        bool showColor = Shader.GetGlobalFloat("_ShowColor") > 0.5f;//tileMap.GetFloat
        if (showColor != EditorGUILayout.Toggle("Show Color?", showColor))
        {
            Shader.SetGlobalFloat("_ShowColor", showColor ? 0 : 1);// tileMap.SetFloat
            SceneView.RepaintAll();
        }

        if (GUILayout.Button("Reload Map")) ReloadMap();
        if (GUILayout.Button("Export Stamps")) ExportStamps();
        if (GUILayout.Button("Export Animations")) ExportStampAnimations();
        if (GUILayout.Button("Export Avatars")) ExportAvatars();
        if (GUILayout.Button("Export Map")) ExportMap();
    }

    void OnFocus() {
        EditorSceneManager.sceneOpened -= SceneChanged;
        EditorSceneManager.sceneOpened += SceneChanged;
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
        SceneView.onSceneGUIDelegate += OnSceneGUI;
    }
    
    void OnDestroy() {
        EditorSceneManager.sceneOpened -= SceneChanged;
        SceneView.onSceneGUIDelegate -= OnSceneGUI;
    }

    void SceneChanged(Scene scene, OpenSceneMode mode) {
        SetTileMap();
    }   
     
    void OnSceneGUI(SceneView sceneView) {
        GameObject selection = Selection.activeGameObject;
        if (selection == null) return;
        GBMapChunk chunk = selection.GetComponentInParent<GBMapChunk>();
        if (chunk == null) return;

        mat.SetPass(0);
        GL.PushMatrix();
        GL.MultMatrix(chunk.transform.localToWorldMatrix);

        // Draw lines
        GL.Begin(GL.LINES);
        for (int i = 0; i <= 16; i++) {
            for (int j = 0; j <= 16; j++) {
                GL.Color(gridColor);
                GL.Vertex3(i*2, 0, 0);
                GL.Vertex3(i*2, 32, 0);
                GL.Vertex3(0, j*2, 0);
                GL.Vertex3(32, j*2, 0);
            }
        }
        GL.End();
        GL.PopMatrix();
    }


    public static int DrawTileMap (Material mat, int tile) {
        Rect r = EditorGUILayout.GetControlRect(false, Screen.width/2); 
        r.height = r.width * 0.5f;
        float x = r.x + r.width * (tile % 16) / 16f;
        float y = r.y + r.height * (tile / 16) / 8f;
        float w = r.width / 16f;
        float h = r.height / 8f;
        if (mat.GetFloat("_IsSprite") > 0.5f) GUI.DrawTexture(r, mat.GetTexture("_SpriteTiles"));
        else GUI.DrawTexture(r, mat.GetTexture("_BackgroundTiles"));
        EditorGUILayout.GetControlRect(false, Screen.width/2); 
        r.y += r.height;
        GUI.DrawTexture(r, mat.GetTexture("_SharedTiles"));
        r.y -= r.height;
        r.height *= 2;
        Vector3 size = new Vector3(w, h, 0);
        Vector3 pos = new Vector3(x, y, 0) + size * 0.5f;
        Handles.color = Color.blue;
        Handles.DrawWireCube(pos, size);

        Event e = Event.current;
        if (e.type == EventType.MouseDown && e.button == 0 && r.Contains(e.mousePosition)) {
            x = 16 * (e.mousePosition.x - r.x) / r.width;
            y = 16 * (e.mousePosition.y - r.y) / r.height;
            return (int)y * 16 + (int)x;
        }
        return tile;
    }

    public static int DrawPaletteMap (Texture tex, int palette) {
        Rect r = EditorGUILayout.GetControlRect(false, Screen.width/4); 
        float y = r.y + r.height * palette / 8f;
        float h = r.height / 8f;
        GUI.DrawTexture(r, tex);
        Vector3 size = new Vector3(r.width, h, 0);
        Vector3 pos = new Vector3(r.x, y, 0) + size * 0.5f;
        Handles.color = Color.blue;
        Handles.DrawWireCube(pos, size);

        Event e = Event.current;
        if (e.type == EventType.MouseDown && e.button == 0 && r.Contains(e.mousePosition)) {
            return (int)(8 * (e.mousePosition.y - r.y) / r.height);
        }
        return palette;
    }

    [MenuItem("GameBoy/Set TileMap From Scene")]
    static void SetTileMap () {
        Renderer obj = Object.FindObjectOfType<Renderer>();
        GBMapEditor window = EditorWindow.GetWindow(typeof(GBMapEditor)) as GBMapEditor;
        if (obj == null || window == null) return;
        window.tileMap = obj.sharedMaterial;
    }

    [MenuItem("GameBoy/Export Scene as Map")]
    static void ExportMap () {
        Scene scene = EditorSceneManager.GetActiveScene();
        string name = GBMapUtils.ConvertToLabel(scene.name );
        string path = EditorUtility.SaveFilePanel("Save Map", lastFolder, name + ".gbmap", "gbmap");
        if (string.IsNullOrEmpty(path)) return;
        lastFolder = Path.GetDirectoryName(path);
        GBMapUtils.SaveScene(scene, path);
    }

    [MenuItem("GameBoy/Export Avatars")]
    static void ExportAvatars () {
        string path = EditorUtility.SaveFilePanel("Save Stamp Animations", lastFolder, "map_avatars.gbavatar", "gbavatar");
        if (string.IsNullOrEmpty(path)) return;
        lastFolder = Path.GetDirectoryName(path);
        List<GBAvatar> avatars = new List<GBAvatar>();
        string[] guids = AssetDatabase.FindAssets(string.Format("t:GBAvatar", typeof(GBAvatar)));
        for( int i = 0; i < guids.Length; i++ )
        {
            string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
            GBAvatar avatar = AssetDatabase.LoadAssetAtPath<GBAvatar>( assetPath );
            if(avatar != null) avatars.Add(avatar);
        }

        GBMapUtils.SaveAvatars(avatars.ToArray(), path);
    }

    [MenuItem("GameBoy/Export Stamp Animations")]
    static void ExportStampAnimations () {
        string path = EditorUtility.SaveFilePanel("Save Stamp Animations", lastFolder, "map_animations.gbanim", "gbanim");
        if (string.IsNullOrEmpty(path)) return;
        lastFolder = Path.GetDirectoryName(path);
        List<GBStampAnimation> anims = new List<GBStampAnimation>();
        string[] guids = AssetDatabase.FindAssets(string.Format("t:GBStampAnimation", typeof(GBStampAnimation)));
        for( int i = 0; i < guids.Length; i++ )
        {
            string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
            GBStampAnimation anim = AssetDatabase.LoadAssetAtPath<GBStampAnimation>( assetPath );
            if(anim != null) anims.Add(anim);
        }

        GBMapUtils.SaveAnims(anims.ToArray(), path);
    }

    [MenuItem("GameBoy/Export Stamps")]
    static void ExportStamps () {
        string path = EditorUtility.SaveFilePanel("Save Stamps", lastFolder, "map_stamps.gbstamp", "gbstamp");
        if (string.IsNullOrEmpty(path)) return;
        lastFolder = Path.GetDirectoryName(path);
        List<GBStamp> stamps = new List<GBStamp>();
        string[] guids = AssetDatabase.FindAssets(string.Format("t:GBStamp", typeof(GBStamp)));
        for( int i = 0; i < guids.Length; i++ )
        {
            string assetPath = AssetDatabase.GUIDToAssetPath( guids[i] );
            GBStamp stamp = AssetDatabase.LoadAssetAtPath<GBStamp>( assetPath );
            if(stamp != null) stamps.Add(stamp);
        }

        SceneSetup[] sceneSetup = EditorSceneManager.GetSceneManagerSetup();
        guids = AssetDatabase.FindAssets("t:Scene");
        for (int i = 0; i < guids.Length; i++) {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            Scene scene = EditorSceneManager.OpenScene(assetPath, OpenSceneMode.Additive); 
            GameObject[] roots = scene.GetRootGameObjects();
            foreach (GameObject root in roots) {
                GBMapText[] mapTexts = root.GetComponentsInChildren<GBMapText>();
                foreach (GBMapText mapText in mapTexts) {
                    if (mapText != null) {
                        GBStamp stamp = mapText.GetStamp();
                        if (stamps.Find((s) => s.name == stamp.name) == null) stamps.Add(stamp);
                    }
                }
            }
        }
        GBMapUtils.SaveStamps(stamps.ToArray(), path);
        EditorSceneManager.RestoreSceneManagerSetup(sceneSetup);
    }

    [MenuItem("GameBoy/Export All Scenes as Maps")]
    static void ExportAllMaps () {
        SceneSetup[] sceneSetup = EditorSceneManager.GetSceneManagerSetup();
        Scene activeScene = EditorSceneManager.GetActiveScene();
        string[] guids = AssetDatabase.FindAssets("t:Scene");
        for (int i = 0; i < guids.Length; i++) {
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            string sceneName = Path.GetFileNameWithoutExtension(assetPath);
            if (sceneName == "Reference") continue;
            Scene scene = EditorSceneManager.OpenScene(assetPath, OpenSceneMode.Additive);
            string name = GBMapUtils.ConvertToLabel(sceneName);
            string path = Path.Combine(lastFolder, name + ".gbmap");
            Debug.Log(path);
            if (!string.IsNullOrEmpty(path)) GBMapUtils.SaveScene(scene, path);
            if (scene != activeScene) EditorSceneManager.CloseScene(scene, true);
        }
        EditorSceneManager.RestoreSceneManagerSetup(sceneSetup);
    }
    
    [MenuItem("GameBoy/Reload Map")]
    static void ReloadMap () {
        GBMapChunk[] chunks = Object.FindObjectsOfType<GBMapChunk>() as GBMapChunk[];
        for (int i = 0; i < chunks.Length; i++) {
            for (int j = i+1; j < chunks.Length; j++) {
                GBMapChunk a = chunks[i];
                GBMapChunk b = chunks[j];
                if (a.x == b.x && a.y == b.y+32) {
                    if (b.north != null) 
                        b.north.south = null;
                    b.north = a;
                    a.south = b;
                }
                if (a.x == b.x+32 && a.y == b.y) {
                    if (b.east != null) 
                        b.east.west = null;
                    b.east = a;
                    a.west = b;
                }
                if (a.x == b.x && a.y == b.y-32) {
                    if (b.south != null) 
                        b.south.north = null;
                    b.south = a;
                    a.north = b;
                }
                if (a.x == b.x-32 && a.y == b.y) {
                    if (b.west != null) 
                        b.west.east = null;
                    b.west = a;
                    a.east = b;
                }
            }
        }
        GBMapObject[] objs = Object.FindObjectsOfType<GBMapObject>() as GBMapObject[];
        foreach (GBMapObject obj in objs) {
            obj.CreateMesh();
            PropertyInfo property = obj.gameObject.GetType().GetProperty("isStatic", BindingFlags.Instance | BindingFlags.Public);
            property.SetValue(obj.gameObject, !obj.isSprite, null);
        }
    }


    [MenuItem("GameBoy/Validate Map")]
    static void ValidateMapData () {
        GBMapChunk[] chunks = Object.FindObjectsOfType<GBMapChunk>() as GBMapChunk[];
        foreach (GBMapChunk chunk in chunks) {
            if (chunk.north != null && chunk.north.south != chunk) {
                Debug.LogError(chunk.name + ".north should not be " + chunk.north.southName);
            }
            if (chunk.south != null && chunk.south.north != chunk) {
                Debug.LogError(chunk.name + ".south should not be " + chunk.south.northName);
            }
            if (chunk.east != null && chunk.east.west != chunk) {
                Debug.LogError(chunk.name + ".east should not be " + chunk.east.westName);
            }
            if (chunk.west != null && chunk.west.east != chunk) {
                Debug.LogError(chunk.name + ".west should not be " + chunk.west.eastName);
            }

            GBMapObject[] objs = chunk.GetComponentsInChildren<GBMapObject>();
            foreach (GBMapObject obj in objs) {
                if (obj is GBMapChunk) {
                    if (obj as GBMapChunk == chunk) continue;
                    else Debug.LogError(obj.name + " should not be a child of " + chunk.name);
                }
                if (obj.x < 0 || obj.y < 0 || obj.x+obj.width > 32 || obj.y+obj.height > 32) {
                    Debug.LogError(obj.name + " is not fully contained within " + chunk.name);
                }

                GBText text = obj.GetComponent<GBText>();
                if (text != null) {
                    if (string.IsNullOrEmpty(text.text)) Debug.LogError(obj.name + " has an empty text component.");
                }

            }
        }
    }
}