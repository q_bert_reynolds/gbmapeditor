﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBDirection))]
[CanEditMultipleObjects]
public class GBDirectionEditor : Editor {

	public override void OnInspectorGUI () {
        SerializedProperty dirProp = serializedObject.FindProperty("directionMask");
        GBDirectionEnum dir = (GBDirectionEnum)dirProp.intValue;
        dirProp.intValue = (int)(GBDirectionEnum)EditorGUILayout.EnumFlagsField("Directions", dir);
        serializedObject.ApplyModifiedProperties();
    }
}
