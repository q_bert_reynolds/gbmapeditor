﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBStampAnimation))]
public class GBStampAnimationEditor : Editor {

    static int frame = 0;
    static float lastFrameChange;
    GBStampAnimation anim { get { return (GBStampAnimation)target; } }

    public override void OnInspectorGUI() {
        if (EditorApplication.timeSinceStartup - lastFrameChange > 0.25f) {
            frame++;
            frame %= anim.frames.Length;
            lastFrameChange = (float)EditorApplication.timeSinceStartup;
        }

        GBStampEditor.DrawStamp(anim.frames[frame].stamp, anim.frames[frame].flipX);
        // base.OnInspectorGUI();
    }

    public override bool RequiresConstantRepaint() {
        return true;
    }
}
