﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(GBStampObject))]
[CanEditMultipleObjects]
public class GBStampObjectEditor : GBMapObjectEditor {

    public static string[] stampNames;
    public static GBStamp[] _stamps;
    public static GBStamp[] stamps {
        get {
            string[] guids = AssetDatabase.FindAssets("t:GBStamp");
            if (_stamps == null || stampNames == null || _stamps.Length != guids.Length) {
                _stamps = new GBStamp[guids.Length];
                stampNames = new string[guids.Length];
                for(int i = 0; i < guids.Length; i++) {
                    string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                    _stamps[i] = AssetDatabase.LoadAssetAtPath<GBStamp>(assetPath);
                    stampNames[i] = _stamps[i].name;
                }
            }
            return _stamps;
        }
    }
    public static int stampID = -1;

    GBStampObject stampObj { get { return (GBStampObject)target; } }

    public override void OnInspectorGUI() {
        stampObj.isSprite = EditorGUILayout.Toggle("Sprite?", stampObj.isSprite);
        
        if (stamps == null || stamps.Length == 0) {
            EditorGUILayout.LabelField("No stamps found.");
            return;
        }

        for (int i = 0; i < stamps.Length; i++) {
            if (stamps[i] == stampObj.stamp) {
                stampID = i;
                break;
            }
        }

        int newStamp = EditorGUILayout.Popup("Stamps", stampID, stampNames);
        if (newStamp != stampID) {
            Undo.RecordObject(stampObj, "Edit Stamp Object");
            stampID = newStamp;
            stampObj.stamp = stamps[stampID];
            stampObj.CreateMesh();
        }

        if (GUI.changed) SnapToGrid();

        if (GUILayout.Button("Split Into Tiles")) SplitIntoTiles();
    }

    public void SplitIntoTiles () {
        for (int j = 0; j < stampObj.height; j++) {
            for (int i = 0; i < stampObj.width; i++) {
                string name = stampObj.name + " " + i + "," + j;
                GameObject g = new GameObject(name);
                Undo.RegisterCreatedObjectUndo(g, "Created " + name);
                g.transform.position = stampObj.transform.position + new Vector3(i,j,0);
                RectTransform rect = g.AddComponent<RectTransform>();
                rect.sizeDelta = Vector2.one;
                g.AddComponent<MeshFilter>();
                MeshRenderer renderer = g.AddComponent<MeshRenderer>();
                renderer.sharedMaterial = stampObj.GetComponent<MeshRenderer>().sharedMaterial;
                SortingGroup sort = g.AddComponent<SortingGroup>();
                sort.sortingOrder = stampObj.GetComponent<SortingGroup>().sortingOrder;
                GBMapObject obj = g.AddComponent<GBMapObject>();
                obj.tile = stampObj.GetTile(i,j);
                obj.palette = stampObj.GetPalette(i,j);
                obj.CreateMesh();
            }
        }
        Undo.DestroyObjectImmediate(stampObj.gameObject);
    }
}
