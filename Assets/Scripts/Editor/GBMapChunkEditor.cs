﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBMapChunk))]
[CanEditMultipleObjects]
public class GBMapChunkEditor : GBMapObjectEditor {

    GBMapChunk chunk { get { return (GBMapChunk)target; } }

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();
        GUI.enabled = false;
        chunk.north = EditorGUILayout.ObjectField("North", chunk.north, typeof(GBMapChunk), true) as GBMapChunk;
        chunk.east = EditorGUILayout.ObjectField("East", chunk.east, typeof(GBMapChunk), true) as GBMapChunk;
        chunk.south = EditorGUILayout.ObjectField("South", chunk.south, typeof(GBMapChunk), true) as GBMapChunk;
        chunk.west = EditorGUILayout.ObjectField("West", chunk.west, typeof(GBMapChunk), true) as GBMapChunk;
        GUI.enabled = true;
    }

    protected override void SnapToGrid () {
        chunk.UpdateRect();
        
        Vector3 pos = chunk.transform.position;
        pos.x = Mathf.RoundToInt(pos.x / 32f) * 32;
        pos.y = Mathf.RoundToInt(pos.y / 32f) * 32;
        pos.z = 0;
        chunk.transform.position = pos;

        chunk.north = null;
        chunk.east = null;
        chunk.south = null;
        chunk.west = null;
        GBMapChunk[] chunks = FindObjectsOfType<GBMapChunk>();
        foreach (GBMapChunk c in chunks) {
            if (c == chunk) continue;
            if (c.x == chunk.x && c.y == chunk.y+32) {
                if (chunk.north != null) 
                    chunk.north.south = null;
                chunk.north = c;
                c.south = chunk;
            }
            if (c.x == chunk.x+32 && c.y == chunk.y) {
                if (chunk.east != null) 
                    chunk.east.west = null;
                chunk.east = c;
                c.west = chunk;
            }
            if (c.x == chunk.x && c.y == chunk.y-32) {
                if (chunk.south != null) 
                    chunk.south.north = null;
                chunk.south = c;
                c.north = chunk;
            }
            if (c.x == chunk.x-32 && c.y == chunk.y) {
                if (chunk.west != null) 
                    chunk.west.east = null;
                chunk.west = c;
                c.east = chunk;
            }
        }
    }

}
