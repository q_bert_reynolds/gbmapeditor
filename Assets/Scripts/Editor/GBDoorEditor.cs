﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using System.IO;

[CustomEditor(typeof(GBDoor))]
public class GBDoorEditor : Editor {

    public static string[] _sceneNames;
    public static string[] scenePaths;
    public static string[] sceneNames {
        get {
            string[] guids = AssetDatabase.FindAssets("t:Scene");
            if (_sceneNames == null || scenePaths == null || _sceneNames.Length != guids.Length) {
                scenePaths = new string[guids.Length];
                _sceneNames = new string[guids.Length];
                for (int i = 0; i < guids.Length; i++) {
                    string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                    scenePaths[i] = assetPath;
                    _sceneNames[i] = Path.GetFileNameWithoutExtension(assetPath);
                }
            }
            return _sceneNames;
        }
    }
    public static int sceneID = -1;

    public string[][,] chunkNames;
    public Vector2Int[] positions;
    public string[] _doorNames;
    public string[] doorNames {
        get {
            if (_doorNames == null || chunkNames == null || positions == null || _doorNames.Length == 0) {
                List<GBDoor> doors = new List<GBDoor>();
                Scene scene = SceneManager.GetActiveScene();
                if (door.sceneName != scene.name) scene = EditorSceneManager.OpenScene(scenePaths[sceneID], OpenSceneMode.Additive); 
                
                GameObject[] roots = scene.GetRootGameObjects();
                foreach (GameObject root in roots) doors.AddRange(root.GetComponentsInChildren<GBDoor>());
    
                doors.Remove(door);
                _doorNames = System.Array.ConvertAll<GBDoor, string>(doors.ToArray(), d => d.name);
                GBMapChunk[] chunks = System.Array.ConvertAll<GBDoor, GBMapChunk>(doors.ToArray(), d => d.GetComponentInParent<GBMapChunk>());
                chunkNames = new string[chunks.Length][,];
                for (int i = 0; i < chunks.Length; i++) {
                    GBMapChunk chunk = chunks[i];
                    chunkNames[i] = new string[,] {//y,x
                        {chunk.northWestName, chunk.northName, chunk.northEastName},
                        {chunk.westName,      chunk.name,      chunk.eastName     },
                        {chunk.southWestName, chunk.southName, chunk.southEastName}
                    };
                }
                positions = System.Array.ConvertAll<GBDoor, Vector2Int>(doors.ToArray(), d => {
                    GBMapObject obj = d.GetComponent<GBMapObject>();
                    return new Vector2Int(obj.x, obj.y);
                });
                if (door.sceneName != SceneManager.GetActiveScene().name) EditorSceneManager.CloseScene(scene, true);
            }
            return _doorNames;
        }
    }
    public int destinationID = -1;

    GBDoor door { get { return (GBDoor)target; } }

    public override void OnInspectorGUI() {
        if (sceneNames.Length == 0) return;

        for (int i = 0; i < sceneNames.Length; i++) {
            if (sceneNames[i] == door.sceneName) {
                sceneID = i;
                break;
            }
        }

        int newScene = EditorGUILayout.Popup("Scene", sceneID, sceneNames);
        if (newScene != sceneID) {
            Undo.RecordObject(door, "Change Scene");
            sceneID = newScene;
            door.sceneName = sceneNames[sceneID];
            _doorNames = null;
        }

        if (doorNames.Length == 0) return;

        for (int i = 0; i < doorNames.Length; i++) {
            if (doorNames[i] == door.destinationName) {
                destinationID = i;
                break;
            }
        }

        int newDestination = EditorGUILayout.Popup("Destination", destinationID, doorNames);
        if (newDestination != destinationID) {
            Undo.RecordObject(door, "Change Destination");
            destinationID = newDestination;
            door.destinationName = doorNames[destinationID];
            SetChunkFromPosition();
        }

        if (destinationID < 0) return;
        Vector2Int pos = EditorGUILayout.Vector2IntField("Offset", door.position-positions[destinationID]);
        if (pos+positions[destinationID] != door.position) {
            Undo.RecordObject(door, "Move Door");
            door.position = pos+positions[destinationID];
            SetChunkFromPosition();
        }

        GBDirectionEnum dir = (GBDirectionEnum)EditorGUILayout.EnumPopup("Direction", door.direction);
        if (dir != door.direction) {
            Undo.RecordObject(door, "Change Door Direction");
            door.direction = dir;
        }
    }

    void SetChunkFromPosition () {
        GBMapObject obj = door.GetComponent<GBMapObject>();
        int cameraX = door.position.x-GBDoor.playerX;
        int cameraY = door.position.y+GBDoor.playerY-obj.height;
        int chunkX = 1;
        int chunkY = 1;
        if (cameraX < 0) chunkX = 0;
        if (cameraX >= 32) chunkX = 2;
        if (cameraY < 0) chunkY = 2;
        if (cameraY >= 32) chunkY = 0;
        door.chunkName = chunkNames[destinationID][chunkY,chunkX];
    }
}
