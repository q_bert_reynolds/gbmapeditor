using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GBLineObject))]
[CanEditMultipleObjects]
public class GBLineObjectEditor : GBMapObjectEditor {

    GBLineObject line { get { return (GBLineObject)target; } }

    public override void OnInspectorGUI () {
        GUIStyle s = new GUIStyle(GUI.skin.box);
        s.normal.textColor = Color.red;
        EditorGUILayout.LabelField("Lines not supported yet.", s);
        base.OnInspectorGUI();
    }
}