using UnityEditor;
using UnityEngine;

public class GBReplaceStampsWizard : ScriptableWizard
{
    public GBStampObject prefab;

    [MenuItem("GameBoy/Replace Stamps")]
    static void CreateWizard() {
        ScriptableWizard.DisplayWizard<GBReplaceStampsWizard>("Replace Stamps", "Replace");
    }

    void OnWizardCreate() {
        if (prefab == null) return;
        GBStampObject[] stamps = GameObject.FindObjectsOfType<GBStampObject>();

        for (int i = stamps.Length-1; i >= 0; i--) {
            GBStampObject stamp = stamps[i];
            if (stamp.stamp != prefab.stamp) continue;
            GBStampObject replacement = PrefabUtility.InstantiatePrefab(prefab) as GBStampObject;
            replacement.transform.position = stamp.transform.position;
            replacement.transform.SetParent(stamp.transform.parent);
            GameObject.DestroyImmediate(stamp.gameObject);
        }
    }
}