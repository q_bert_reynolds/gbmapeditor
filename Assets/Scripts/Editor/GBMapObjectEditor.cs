﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

[CustomEditor(typeof(GBMapObject))]
[CanEditMultipleObjects]
public class GBMapObjectEditor : Editor {


    GBMapObject gbObj { get { return (GBMapObject)target; } }
    Material mat { get { return gbObj.GetComponent<MeshRenderer>().sharedMaterial; } }
    Texture palTex { get { return gbObj.GetComponent<MeshRenderer>().sharedMaterial.GetTexture("_Palette"); } }
    
    protected SerializedProperty tileProp;
    protected SerializedProperty paletteProp;
    protected SerializedProperty spriteProp;
    protected SerializedProperty flipXProp;
    protected SerializedProperty flipYProp;

    void OnEnable() {
        spriteProp = serializedObject.FindProperty ("isSprite");
        tileProp = serializedObject.FindProperty ("tile");
        paletteProp = serializedObject.FindProperty ("palette");
        flipXProp = serializedObject.FindProperty ("flipX");
        flipYProp = serializedObject.FindProperty ("flipY");
        Undo.undoRedoPerformed += UndoCallback;
    }

    void OnDisable () {
        Undo.undoRedoPerformed -= UndoCallback;
    }

    void UndoCallback () {
        if (target == null) return;
        gbObj.UpdateRect();
        gbObj.CreateMesh();
    }

    public override void OnInspectorGUI() {
        EditorGUILayout.PropertyField(spriteProp, new GUIContent("Sprite?"));
        if (GUI.changed) {
            PropertyInfo property = gbObj.gameObject.GetType().GetProperty("isStatic", BindingFlags.Instance | BindingFlags.Public);
            property.SetValue(gbObj.gameObject, !gbObj.isSprite, null);
        }
        serializedObject.ApplyModifiedProperties();
        if (!spriteProp.hasMultipleDifferentValues && spriteProp.boolValue) {
            EditorGUILayout.PropertyField(flipXProp, new GUIContent("Flip X"));
            EditorGUILayout.PropertyField(flipYProp, new GUIContent("Flip Y"));
            serializedObject.ApplyModifiedProperties();
            if (GUI.changed) UpdateUVs();
        }
        DrawTilePicker();
        DrawPalettePicker();
        if (GUI.changed) SnapToGrid();
    }

    protected void DrawTilePicker () {
        if (mat && !tileProp.hasMultipleDifferentValues) {
            mat.SetFloat("_IsSprite", gbObj.isSprite ? 1 : 0);
            int tile = GBMapEditor.DrawTileMap(mat, tileProp.intValue);
            if (tile != gbObj.tile) {
                tileProp.intValue = tile;
                serializedObject.ApplyModifiedProperties();
                UpdateUVs();
            }
            mat.SetFloat("_IsSprite", 0);
        }
    }

    protected void DrawPalettePicker () {
        if (palTex && !paletteProp.hasMultipleDifferentValues) {
            int pal = GBMapEditor.DrawPaletteMap(palTex, paletteProp.intValue);
            if (pal != gbObj.palette) {
                paletteProp.intValue = pal;
                serializedObject.ApplyModifiedProperties();
                UpdateColors();
            }
        }
    }

    protected void UpdateUVs () {
        foreach (Object obj in targets) {
            ((GBMapObject)obj).UpdateUVs();
        }
    }

    protected void UpdateColors () {
        foreach (Object obj in targets) {
            ((GBMapObject)obj).UpdateColors();
        }
    }

    public virtual void OnSceneGUI() {
        Event e = Event.current;
        if (e.type == EventType.MouseDrag) {
            gbObj.UpdateRect();
        }
        else if (e.rawType == EventType.MouseUp) {
            SnapToGrid();
        }
    }

    protected virtual void SnapToGrid () {
        gbObj.UpdateRect();
        
        Undo.SetTransformParent(gbObj.transform, null, "Clear parent"); 
        GBMapChunk[] chunks = FindObjectsOfType<GBMapChunk>();
        foreach (GBMapChunk chunk in chunks) {
            if ((!gbObj.isSprite && chunk.Contains(gbObj.x, gbObj.y)) || 
                (gbObj.isSprite && chunk.Contains(gbObj.x/8, gbObj.y/8))) {
                Undo.SetTransformParent(gbObj.transform, chunk.transform, "Set parent to chunk"); 
                break;
            }
        }
    }

}
