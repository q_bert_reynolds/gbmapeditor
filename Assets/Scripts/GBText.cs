﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBText : GBData {

	[Multiline(6)] public string text = "";

    public override int dataSize { get { return text.Length + 1; } }
    public override string collisionString { get { return "MAP_COLLISION_TEXT"; } }

    public override string ToString () {
        return GBMapUtils.SanitizeString(text);
    }
}
