﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GBStamp", menuName = "GameBoy/Stamp")]
public class GBStamp : ScriptableObject {

    public int width = 2;
    public int height = 2;
    public int[] tiles;
    public int[] palettes;//props for sprites
    public bool isSprite = false;
    public Material mat;
    
    public bool hasUniformPalette {
        get {
            int palette = GetPalette(0,0);
            for (int i = 1; i < palettes.Length; i++) {
                if (palette != palettes[i]) return false;
            }
            return true;
        }
    }

    public int GetTile (int x, int y) {
        if (x < 0 || y < 0 || x >= width || y >= height) return 0;
        if (tiles == null || tiles.Length != width*height) tiles = new int[width*height];
        int index = (height-1-y)*width+x;
        if (index < tiles.Length) return tiles[index];
        return 0;
    }

    public int GetProps (int x, int y) {
        if (x < 0 || y < 0 || x >= width || y >= height) return 0;
        if (palettes == null || palettes.Length != width*height) palettes = new int[width*height];
        int index = (height-1-y)*width+x;
        if (index < palettes.Length) return palettes[index];
        return 0;
    }

    public int GetPalette (int x, int y) {
        return GetProps(x,y) & 7;//only lowest 3 bits are palette data
    }

    public bool GetXFlip (int x, int y) {
        return (GetProps(x,y) & (1 << 5)) != 0;
    }

    public bool GetYFlip (int x, int y) {
        return (GetProps(x,y) & (1 << 6)) != 0;
    }

    public override string ToString() {
        string s = "  DB " + width + ";width\n";
        s += "  DB " + height + ";height\n";
        s += ".tiles\n";
        for (int j = 0; j < height; j++) {
            s += "  DB ";
            for (int i = 0; i < width; i++) {
                s += GetTile(i, height-1-j).ToString().PadLeft(3, ' ');
                if (i < width-1) s += ", ";
            }
            s += "\n";
        }
        if (hasUniformPalette && !isSprite) {
            s += ".palette\n";
            s += "  DB " + (GetPalette(0,0) | 128) + "\n";
        }
        else {
            s += isSprite ? ".props\n" : ".palettes\n";
            for (int j = 0; j < height; j++) {
                s += "  DB ";
                for (int i = 0; i < width; i++) {
                    if (isSprite) s += GetProps(i, height-1-j).ToString();
                    else s += GetPalette(i, height-1-j).ToString();
                    if (i < width-1) s += ", ";
                }
                s += "\n";
            }
        }
        return s;
    }
}
