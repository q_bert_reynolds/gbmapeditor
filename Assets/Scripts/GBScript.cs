﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBScript : GBData {

    public string addressLabel;

    public override int dataSize { get { return 3; } }
    public override string collisionString { get { return "MAP_COLLISION_SCRIPT"; } }

    public override string ToString () {
        string label = GBMapUtils.ConvertToLabel(addressLabel);
        string s = "  DB BANK(" + label + ")\n";
        s += "  DW " + label + "\n";
        return s;
    }
}
