using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GBBehaviourEnum {
    Stationary,
    Wander
}

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class GBAvatarObject : GBMapObject {

    public GBDirectionEnum direction = GBDirectionEnum.Down;
    public GBBehaviourEnum behaviour = GBBehaviourEnum.Stationary;
    public GBAvatar avatar;

    public override int width { get { return 2; } }
    public override int height { get { return 2; } }
    public override int asmSize { get { return 5; } }

    public GBFrame frame {
        get {
            if (avatar == null) return null;
            return avatar.GetFrame(direction);
        }
    }

    public override int GetTile (int x, int y) {
        if (frame == null || frame.stamp == null) return base.GetTile(x,y);
        return frame.stamp.GetTile(x,y);
    }

    public override int GetProps(int x, int y) {
        if (frame == null || frame.stamp == null) return base.GetProps(x,y);
        return frame.stamp.GetProps(x,y);
    }

    public override bool GetXFlip(int x, int y) {
        if (frame == null || frame.stamp == null) return base.GetXFlip(x,y);
        return frame.stamp.GetXFlip(x,y);
    }

    public string GetExtraData() {
        string s = "";
        switch (direction) {
            case GBDirectionEnum.Left:
                s += "MAP_AVATAR_LEFT";
                break;
            case GBDirectionEnum.Right:
                s += "MAP_AVATAR_RIGHT";
                break;
            case GBDirectionEnum.Up:
                s += "MAP_AVATAR_UP";
                break;
            case GBDirectionEnum.Down:
            default:
                s += "MAP_AVATAR_DOWN";
                break;
        }
        s += " | ";
        switch (behaviour) {
            case GBBehaviourEnum.Stationary:
                s += "MAP_AVATAR_STATIONARY";
                break;
            case GBBehaviourEnum.Wander:
            default:
                s += "MAP_AVATAR_WANDER";
                break;
        }
        return s;
    }

    public override string ToString () {
        if (!GetComponent<Renderer>().enabled) return base.ToString();
        string s = "\n";
        s += "  DB MAP_OBJ_AVATAR"+collisionString+";"+name+"\n";
        s += "  DB " + x + ";x\n";
        s += "  DB " + gby + ";y\n";
        s += "  DW GBAvatar" + GBMapUtils.ConvertToLabel(avatar.name) + "\n";
        s += "  DB " + GetExtraData() + "\n";
        
        return s;
    }
    
}