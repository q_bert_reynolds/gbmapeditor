using UnityEngine;

public class GBData : MonoBehaviour {
    
    public virtual int dataSize { get { return 1; } }
    public virtual string collisionString { get { return "MAP_COLLISION_SOLID"; } }
}