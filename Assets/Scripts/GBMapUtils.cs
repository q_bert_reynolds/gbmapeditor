﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;

public class GBMapUtils {

    public static int UnityToGBColorChannel (float channel) { return Mathf.RoundToInt(31 * channel); }

    public static Dictionary<char, char> charReplace = new Dictionary<char, char>() {
        { 'ℙ', 'P' },             // pitcher
        { 'ℂ', 'C' },             // catcher
        { '¹', '1' },             // first
        { '²', '2' },             // second
        { '³', '3' },             // third
        { 'ş', 'S' },             // short
        { 'ļ', 'L' },             // left
        { 'ç', 'C' },             // center
        { 'ŗ', 'R' },             // right
        { '◌', 'D' },            // dotted circle
        { '⚾', '0' },           // baseball
        { 'ț', 'T' },            // to
        { 'ᚠ', 'A' },            // age
        { '↵', 'R' },            // end/return
        { 'é', 'E' },
    };

    public static string SanitizeString (string str) {
        return str.Replace("\n", "\\n");
    }

    public static string ConvertToLabel (string str) {
        string s = "";
        foreach (char c in str) {
            if (char.IsLetterOrDigit(c)) s += Normalize(c);
        }
        return s;
    }

    public static string ConvertToConstant (string str) {
        string s = "";
        foreach (char c in str) {
            if (c == ' ') s += '_';
            else if (char.IsLetterOrDigit(c)) s += Normalize(c);
        }
        return s.ToUpper();
    }

    public static char Normalize (char s) {        
        var chars = s.ToString()
            .Normalize(NormalizationForm.FormD)
            .ToCharArray()
            .Where(c=> CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
            .ToArray();

        s = chars[0];
        if (charReplace.ContainsKey(s)) s = charReplace[s];
        return s;
    }

    public static void SaveStamps (GBStamp[] stamps, string path) {
        int stampBytes = 0;
        string stampData = "";
        foreach (GBStamp stamp in stamps) {
            if (stamp == null) continue;
            int bytes = 2 + stamp.width * stamp.height;
            if (stamp.hasUniformPalette) bytes++;
            else bytes += stamp.width * stamp.height;
            stampBytes += bytes;
            stampData += "GBStamp"+ConvertToLabel(stamp.name) + ": ;"+bytes+" bytes\n" + stamp.ToString() + "\n";
        }

        stampData =  "GBStamps:: ;"+stampBytes+" bytes\n" + stampData;
        File.WriteAllText(path, stampData);
    }

    public static void SaveAnims (GBStampAnimation[] anims, string path) {
        int animBytes = 0;
        string animData = "";
        foreach (GBStampAnimation anim in anims) {
            if (anim == null) continue;
            int bytes = 5 * anim.frames.Length;
            animBytes += bytes;
            animData += "GBStampAnimation"+ConvertToLabel(anim.name) + ": ;"+bytes+" bytes\n" + anim.ToString() + "\n";
        }

        animData =  "GBStampAnimations:: ;"+animBytes+" bytes\n" + animData;
        File.WriteAllText(path, animData);
    }

    public static void SaveAvatars (GBAvatar[] avatars, string path) {
        int avatarBytes = 0;
        string avatarData = "";
        foreach (GBAvatar avatar in avatars) {
            if (avatar == null) continue;
            int bytes = 8;
            avatarBytes += bytes;
            avatarData += "GBAvatar"+ConvertToLabel(avatar.name) + ": ;"+bytes+" bytes\n" + avatar.ToString() + "\n";
        }

        avatarData =  "GBAvatars:: ;"+avatarBytes+" bytes\n" + avatarData;
        File.WriteAllText(path, avatarData);
    }

    public static void SaveScene (Scene scene, string path) {
        int totalBytes = 65;//64 for palette, 1 for texture id
        string mapPrefix = "MAP_"+ConvertToConstant(scene.name);
        string mapLabel =  "Map"+ConvertToLabel(scene.name);
        string mapPalettesLabel = mapLabel+"Palettes";
        string mapDoorsLabel = mapLabel+"Doors";
        string mapTextLabel = mapLabel+"Text";
        string mapChunksLabel = mapLabel+"Chunks";
        string mapScriptsLabel = mapLabel+"Scripts";

        GameObject[] roots = scene.GetRootGameObjects();
        List<GBMapChunk> chunks = new List<GBMapChunk>();
        foreach (GameObject root in roots) {
            GBMapChunk chunk = root.GetComponent<GBMapChunk>();
            if (chunk != null) chunks.Add(chunk);
        }
        string paletteData = mapPalettesLabel+": ;64 bytes\n";
        Texture2D pal = chunks[0].GetComponent<MeshRenderer>().sharedMaterial.GetTexture("_Palette") as Texture2D;
        for (int j = 7; j >= 0; j--) {
            for (int i = 0; i < 4; i++) {
                Color c = pal.GetPixel(i*8+4, j*8+4);
                paletteData += "  RGB ";
                paletteData += UnityToGBColorChannel(c.r) + ", ";
                paletteData += UnityToGBColorChannel(c.g) + ", ";
                paletteData += UnityToGBColorChannel(c.b) + "\n";
            }
            paletteData += "\n";
        }

        Vector3[] corners = new Vector3[4];
        GBMapObject[] mapObjects = GameObject.FindObjectsOfType<GBMapObject>();
        System.Array.Sort(mapObjects, (a,b) => {
            int sort = 0;
            SortingGroup groupA = a.GetComponent<SortingGroup>();
            SortingGroup groupB = b.GetComponent<SortingGroup>();
            if (groupA != null && groupB != null) {
                sort = groupA.sortingOrder.CompareTo(groupB.sortingOrder);
            }
            if (sort == 0) {
                int aY = Mathf.RoundToInt(a.transform.position.y);
                int bY = Mathf.RoundToInt(b.transform.position.y);
                sort = aY.CompareTo(bY);
            }
            return sort;
        });
        Rect[] rects = System.Array.ConvertAll<GBMapObject, Rect> (mapObjects, obj => {
            (obj.transform as RectTransform).GetWorldCorners(corners);
            return Rect.MinMaxRect(
                Mathf.RoundToInt(corners[0].x),
                Mathf.RoundToInt(corners[0].y),
                Mathf.RoundToInt(corners[2].x),
                Mathf.RoundToInt(corners[2].y)
            );
        });

        int mapChunksBytes = 0;
        string mapConstants = "";
        int jumpTableSize = chunks.Count * 2;

        string chunkJumpTable = mapChunksLabel+": ;"+jumpTableSize+" bytes\n";
        string mapChunksData = "";

        int doorBytes = 0;
        string doorData = "";
        int doorIndex = 0;

        int textIndex = 0;
        List<string> textConstants = new List<string>();
        string textConstantsString = "";
        string textData = "";
        int textBytes = 0;

        int scriptIndex = 0;
        string scriptData = "";
        int scriptBytes = 0;
        for (int i = 0; i < chunks.Count; i++) {
            GBMapChunk chunk = chunks[i];
            mapConstants += mapPrefix + "_CHUNK_" + ConvertToConstant(chunk.name) + " EQU " + i.ToString() + "\n";

            string chunkLabel = mapLabel+"Chunk"+ConvertToLabel(chunk.name);
            chunkJumpTable += "  DW " + chunkLabel + "\n";

            // 2 bytes for fill tile and palette
            // 8 bytes for neighboring chunks
            int chunkBytes = 10;
            mapChunksData += chunkLabel + ":\n";
            mapChunksData += ".tile:    DB " + chunk.tile + "\n";
            mapChunksData += ".palette: DB " + chunk.palette + "\n";
            mapChunksData += ".north:   DB " + mapPrefix + "_CHUNK_"+ConvertToConstant(chunk.northName) + "\n";
            mapChunksData += ".east:    DB " + mapPrefix + "_CHUNK_"+ConvertToConstant(chunk.eastName) + "\n";
            mapChunksData += ".south:   DB " + mapPrefix + "_CHUNK_"+ConvertToConstant(chunk.southName) + "\n";
            mapChunksData += ".west:    DB " + mapPrefix + "_CHUNK_"+ConvertToConstant(chunk.westName) + "\n";

            int spritesBytes = 0;
            string chunkSpritesLabel = mapLabel+"Sprites"+ConvertToLabel(chunk.name);
            string spritesData = chunkSpritesLabel + ":\n";
            
            (chunk.transform as RectTransform).GetWorldCorners(corners);
            Rect chunkRect = Rect.MinMaxRect(
                Mathf.RoundToInt(corners[0].x),
                Mathf.RoundToInt(corners[0].y),
                Mathf.RoundToInt(corners[2].x),
                Mathf.RoundToInt(corners[2].y)
            );
            for (int j = 0; j < mapObjects.Length; j++) {
                GBMapObject obj = mapObjects[j];
                if (obj is GBMapChunk) continue;
                if (obj is GBLineObject) continue;//not supported yet
                if (!chunkRect.Overlaps(rects[j])) continue;
                
                int dataSize = obj.asmSize;
                string dataString = obj.ToString();
                
                GBData extraData = obj.GetComponent<GBData>();
                if (extraData != null) {
                    dataSize++;
                    
                    if (extraData is GBText) {
                        GBText text = extraData as GBText;
                        string textConstant = mapPrefix+"_TEXT_"+ConvertToConstant(text.text);
                        if (!textConstants.Contains(textConstant)) {
                            textConstants.Add(textConstant);
                            textConstantsString += textConstant + " EQU " + textIndex.ToString() + "\n";
                            textIndex++;
                            textBytes += text.dataSize;
                            textData += "  DB \"" + text.ToString() + "\",0\n";
                        }
                        dataString += "  DB " + textConstant + "; " + text.ToString() + "\n";
                    }
                    else if (extraData is GBDirection) {
                        dataString += "  DB " + ((int)(extraData as GBDirection).directionMask).ToString() + ";direction mask\n";
                    }
                    else if (extraData is GBDoor) {
                        GBDoor door = (extraData as GBDoor);
                        doorBytes += door.dataSize;
                        doorData += door.ToString();
                        dataString += "  DB " + doorIndex + ";door\n";
                        doorIndex++;
                    }
                    else if (extraData is GBScript) {
                        GBScript script = extraData as GBScript;
                        dataString += "  DB " + scriptIndex + ";" + script.addressLabel + "\n";
                        scriptBytes += script.dataSize;
                        scriptData += script.ToString();
                        scriptIndex++;
                    }
                }

                if (obj.isSprite) {
                    spritesData += dataString;
                    spritesBytes += dataSize;
                }
                else {
                    mapChunksData += dataString;
                    chunkBytes += dataSize;
                }
            }
            chunkBytes++;//account for terminating byte
            spritesBytes++;
            mapChunksBytes += chunkBytes + spritesBytes;
            mapChunksData += "\n  DB 0;END " + ConvertToLabel(chunk.name) + " background objects, " + chunkBytes + " bytes\n\n";
            spritesData += "\n  DB 0;END " + ConvertToLabel(chunk.name) + " sprite objects, " + spritesBytes + " bytes\n\n";
            mapChunksData += spritesData;
        }
        mapChunksData = ";Map Chunks, " + mapChunksBytes + " bytes\n" + mapChunksData;
        totalBytes += mapChunksBytes;

        doorData = mapDoorsLabel+": ;" + doorBytes.ToString() + " bytes\n" + doorData + "\n";
        textData = mapTextLabel+": ;" + textBytes.ToString() + " bytes\n" + textData + "\n";
        scriptData = mapScriptsLabel+": ;" + scriptBytes.ToString() + " bytes\n" + scriptData + "\n";
        totalBytes += jumpTableSize + textBytes + doorBytes;

        string headerString = mapLabel+":: ;" + totalBytes + " bytes total\n"
                            + "  DB "+mapPrefix+"\n"
                            + "  DW "+mapPalettesLabel+" ;64 bytes\n"
                            + "  DW "+mapTextLabel+" ;"+textBytes+"\n"
                            + "  DW "+mapDoorsLabel+" ;"+doorBytes+"\n"
                            + "  DW "+mapChunksLabel+" ;"+(mapChunksBytes+jumpTableSize)+" bytes\n"
                            + "  DW "+mapScriptsLabel+" ;"+scriptBytes+" bytes\n"
                            + "\n";

        string mapData = headerString
                       + paletteData
                       + textConstantsString
                       + textData
                       + doorData
                       + mapConstants
                       + "\n" + chunkJumpTable + "\n" + mapChunksData
                       + scriptData;
        File.WriteAllText(path, mapData);
    }
}
