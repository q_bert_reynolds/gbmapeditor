﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBDoor : GBData {

    public string sceneName;
    public string chunkName;
    public string destinationName;
    public Vector2Int position;
    public GBDirectionEnum direction = GBDirectionEnum.Down;

    public override int dataSize { get { return 5; } }

    public const int playerX = 10;
    public const int playerY = 23;
    public override string ToString () {
        int x = (((position.x-playerX+32)%32)/2)*2;
        int y = ((32-((position.y-playerY+32)%32))/2)*2;
        string s = ".door"+GBMapUtils.ConvertToLabel(name)+"\n";
        s += "  DB MAP_"+GBMapUtils.ConvertToConstant(sceneName)+"\n";
        s += "  DB MAP_"+GBMapUtils.ConvertToConstant(sceneName)+"_CHUNK_"+GBMapUtils.ConvertToConstant(chunkName)+"\n";
        s += "  DB "+x+";x="+position.x+"\n";
        s += "  DB "+y+";y="+(32-position.y)+"\n";
        s += "  DB "+((int)direction).ToString()+";direction\n";
        return s;
    } 

    public override string collisionString {
        get { return "MAP_COLLISION_DOOR"; }
    }
}
